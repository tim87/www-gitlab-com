---
layout: markdown_page
title: "Category Vision - Pages"
---

- TOC
{:toc}

## Pages

Pages allows you to automatically document and deliver your content through your delivery pipelines with built-in content hosting support.

- [Issue List](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=pages&sort=milestone)
- [Overall Vision](/direction/release)
- [UX Research](https://gitlab.com/groups/gitlab-org/-/epics/594)

Reach out to PM Jason Lenny ([E-Mail](mailto:jason@gitlab.com) / 
[Twitter](https://twitter.com/j4lenn)) if you'd like to provide feedback or ask 
questions about what's coming.

### Overall Prioritization

Pages is not our most overall strategic feature, but it's a well-loved feature and one that people really enjoy engaging with as part of the GitLab experience. We do not expect to provide a market-leading solution in this space for hosting all kinds of static websites, but one which serves the purpose of publishing static web content that goes with your releases.

## What's Next & Why

[gitlab-ce#47857](https://gitlab.com/gitlab-org/gitlab-ce/issues/47857) is a very small issue which will make GitLab pages templates easier to use.

After this, [gitlab-ce#28996](https://gitlab.com/gitlab-org/gitlab-ce/issues/28996) (automatic certificate renewal) is our most popular issue and one that can be quite annoying. We're excited to address this next in order to make using Pages easier and require less maintenance.

## Competitive Landscape

Apart from [gitlab-ce#28996](https://gitlab.com/gitlab-org/gitlab-ce/issues/28996) (certificate renewal) which is highly popular in almost every category here, getting started with Pages in GitLab is harder than it should be. [gitlab-ce#47857](https://gitlab.com/gitlab-org/gitlab-ce/issues/47857) makes this easier by adding Pages templates to the new project dialog.

## Analyst Landscape

We do not engage with analysts in the Pages space, so this is N/A.

## Top Customer Success/Sales Issue(s)

The most popular customer issue is [gitlab-ce#28996](https://gitlab.com/gitlab-org/gitlab-ce/issues/28996), which makes setting up and renewing certificates automatic for your Pages site. This is an ongoing hassle once you set up your Pages site since renewals in particular are frequent and manual. Solving this problem will make Pages even more automatic and easy to use.

## Top Customer Issue(s)

Aligned with what CS is seeing, [gitlab-ce#28996](https://gitlab.com/gitlab-org/gitlab-ce/issues/28996) (certificate renewal) is our top customer issue.

## Top Internal Customer Issue(s)

Similar to many other categories in the vision here at the moment, internal customers have been most frequently raising [gitlab-ce#28996](https://gitlab.com/gitlab-org/gitlab-ce/issues/28996) (certificate renewal) as the biggest irritation in working with pages.

## Top Vision Item(s)

From a vision standpoint, adding Review Apps for Pages ([gitlab-ce#26621](https://gitlab.com/gitlab-org/gitlab-ce/issues/26621)) is interesting because it allows for more sophisticated development flows involving testing, where at the moment the only environment that GitLab understands is production. This would level up our ability for Pages to be a more mission-critical part of projects and groups.
