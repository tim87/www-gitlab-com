---
layout: markdown_page
title: "Triage Operations"
---

## On this page
{:.no_toc}

- TOC
{:toc}

Any GitLab team member can triage issues. Keeping the number of un-triaged issues low is essential for maintainability, and is our collective responsibility.

We have implemented automation and tooling to handle this at scale and distribute the load to each team.

## Triage Packages

A [triage package](https://gitlab.com/gitlab-org/quality/team-tasks/issues/35) is an issue containing a checklist of un-triaged tasks. Each task corresponds to an issue that needs labels, prioritization and/or scheduling.

### Current Packages

TBD 

## Resources

* [Issue Triage Policies](/handbook/engineering/issue-triage/).
* Chat channels; we use our chat internally as a realtime communication tool:
  * [#triage](https://gitlab.slack.com/messages/triage): general triage team channel.
  * [#gitlab-issue-feed](https://gitlab.slack.com/messages/gitlab-issue-feed) - Feed of all GitLab-CE issues
  * [#support-tracker-feed](https://gitlab.slack.com/messages/support-tracker-feed) - Feed of the GitLab.com Support Tracker
  * [#mr-coaching](https://gitlab.slack.com/messages/mr-coaching): for general conversation about Merge Request coaching.
  * [#opensource](https://gitlab.slack.com/messages/opensource): for general conversation about Open Source.