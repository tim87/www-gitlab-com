---
layout: markdown_page
title: "Competitive Intelligence"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## What is competitive intelligence?
Competitive analysis to better position GitLab in our go-to-market efforts.

## Market analysis
* [Application Security market overview](application-security/#market-overview)
* [Application Security competitor scope](application-security/#competitor-scope)
* [CI/CD Primer](cicd/)

## DevOps Tools
GitLab exists in an ecosystem of [DevOps tools](/devops-tools) and might need to interact with any number of these tools. Many have over-lapping capabilities, but that does not mean that we necessarily directly compete with them. A user would need to patch together multiple solutions from this list in order to get all the functionality that is built-in to GitLab as a [single application for end-to-end DevOps](https://about.gitlab.com/). Other places that list tools in various DevOps categories include [CA](https://assessment-tools.ca.com/tools/continuous-delivery-tools/en?embed), [XebiaLabs](https://xebialabs.com/periodic-table-of-devops-tools/), and [CNCF](https://landscape.cncf.io/)
