---
layout: markdown_page
title: "Inbound BDR"
---
---

## On this page
{:.no_toc}

- TOC
{:toc}

This page has been deprecated and moved to [Sales Development](/handbook/marketing/marketing-sales-development/sdr/).
